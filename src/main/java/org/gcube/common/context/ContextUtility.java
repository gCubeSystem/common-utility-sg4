package org.gcube.common.context;

import org.gcube.common.security.providers.SecretManagerProvider;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ContextUtility {

	public static String getCurrentContextFullName() {
		String context = SecretManagerProvider.get().getContext();
		return context;
	}
	
}
