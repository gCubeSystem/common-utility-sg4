package org.gcube.common.http;

import java.util.Map;

import org.gcube.common.gxhttp.request.GXHTTPStringRequest;
import org.gcube.common.security.providers.SecretManagerProvider;
import org.gcube.common.security.secrets.Secret;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GXHTTPUtility {

	public static GXHTTPStringRequest getGXHTTPStringRequest(String address) {
		GXHTTPStringRequest gxHTTPStringRequest = GXHTTPStringRequest.newRequest(address);
		Secret secret = SecretManagerProvider.get();
		Map<String, String> authorizationHeaders = secret.getHTTPAuthorizationHeaders();
		for(String key : authorizationHeaders.keySet()) {
			gxHTTPStringRequest.header(key, authorizationHeaders.get(key));
		}
		return gxHTTPStringRequest;
	}
	
}
