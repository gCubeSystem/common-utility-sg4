This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Common Utility For Smartgears 4.X.X

## [v1.0.1-SNAPSHOT]

- porting to changes of common-security

## [v1.0.0-SNAPSHOT]

- First Version

